package com.brom.priorityqueue;

import java.util.Queue;
import java.util.Random;

public class Main {
  private static String[] testData = {"івфаіфва", "фвафвіа", "фавіфав", "sadfa", "afsdfad", "afds", "-afd", "afds", "adf"};
  public static void main(String[] args) {
    PriorityQueue<String> myPriorityQueue = new PriorityQueue<>(String::compareTo);
    java.util.PriorityQueue<String> javaPriorityQueue = new java.util.PriorityQueue<>(String::compareTo);
    initQueue(myPriorityQueue);
    initQueue(javaPriorityQueue);
    printQueue(myPriorityQueue);
    printQueue(javaPriorityQueue);
    removeElements(myPriorityQueue, javaPriorityQueue);
//    myPriorityQueue.remove();
//    if (javaPriorityQueue.add("qqqq")) {
//      System.out.println("ok");
//    } else {
//      System.out.println("fail");
//    }
    System.out.println(myPriorityQueue.poll());
//    myPriorityQueue.add(2);
//    myPriorityQueue.add(5);
//    myPriorityQueue.add(3);
//    printQueue(myPriorityQueue);
//    if (myPriorityQueue.contains(4)) {
//      System.out.println("qqq");
//    }
//    if (myPriorityQueue.contains(3)) {
//      System.out.println("ee");
//    }
  }
  private static void initQueue(Queue queue) {
    for (int i = 0; i < testData.length; i++) {
      queue.add(testData[i]);
    }
  }
  private static void printQueue(Queue queue) {
    System.out.println("------" + queue.getClass().getName() + "---------");
    queue.forEach(e -> {
      System.out.println(e);
    });
  }
  private static void removeElements(Queue q1, Queue q2) {
    for (int i = testData.length - 1; i >= 0; i--) {
      System.out.print("q1 = " + q1.remove());
      System.out.print("; q2 = " + q2.remove() + "\n");
    }
  }
}
