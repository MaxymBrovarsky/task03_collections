package com.brom.priorityqueue;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class PriorityQueue<T extends Comparable<T>> extends AbstractQueue<T> {
  private ArrayList<T> elements;
  private Comparator<T> comparator;
  public PriorityQueue() {
    this.elements = new ArrayList<T>();
    this.comparator = Comparator.naturalOrder();
  }
  public PriorityQueue(Comparator<T> comparator) {
    this();
    this.comparator = comparator;
  }

  @Override
  public Iterator iterator() {
    return elements.iterator();
  }

  public boolean add(T element) {
    return this.offer(element);
  }

  public boolean offer(T o) {
    if (this.elements.add(o)) {
      Collections.sort(this.elements, comparator);
      return true;
    } else {
      return false;
    }
  }

  public T remove() {
    return this.elements.remove(this.elements.size() - 1);
  }
  public void clear() {
    this.elements.clear();
  }
  public T peek() {
    return this.elements.get(this.elements.size() - 1);
  }
  public int size() {
    return this.elements.size();
  }
  public Object[] toArray() {
    return elements.toArray();
  }
  public T poll() {
    if (elements.isEmpty()) {
      return null;
    }
    return this.elements.remove(this.elements.size() - 1);
  }
  public boolean contains(Object o) {
    return this.elements.contains(o);
  }
}
