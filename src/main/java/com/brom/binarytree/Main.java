package com.brom.binarytree;

import java.util.Collection;
import java.util.Collections;

public class Main {
  public static void main(String[] args) {
    BinaryTree<Integer, Integer> binaryTree = new BinaryTree<>();
    System.out.println(binaryTree.put(50, 1));
    System.out.println(binaryTree.put(75, 2));
    System.out.println(binaryTree.put(62, 3));
    System.out.println(binaryTree.put(87, 4));
    System.out.println(binaryTree.put(77, 5));
    System.out.println(binaryTree.put(79, 6));
    System.out.println(binaryTree.put(93, 7));
    System.out.println("-------------------------");
    binaryTree.print();
    System.out.println("-------------------------");
    System.out.println(binaryTree.get(-7));
    System.out.println("-------------------------");
    System.out.println(binaryTree.containsKey(200));
    System.out.println("-------------------------");
    System.out.println(binaryTree.containsValue(2000));
    System.out.println("-------------------------");
    System.out.println(binaryTree.remove(75));
    System.out.println("-------------------------");
    System.out.println(binaryTree.keySet());
    System.out.println("-------------------------");
    System.out.println(binaryTree.values());
  }
}
