package com.brom.deque;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;

public class MyDeque<T> implements Deque<T> {
  private ArrayList<T> elements;
  private int headIndex;
  private int tailIndex;
  public MyDeque() {
    this.elements = new ArrayList<T>();
    this.headIndex = 0;
    this.tailIndex = 0;
  }
  @Override
  public void addFirst(T t) {
    if (t == null) {
      throw new NullPointerException();
    }
    this.elements.add(headIndex, t);
    tailIndex++;
  }

  @Override
  public void addLast(T t) {
    if (t == null) {
      throw new NullPointerException();
    }
    this.elements.add(tailIndex, t);
    tailIndex++;
  }

  @Override
  public boolean offerFirst(T t) {
    addFirst(t);
    return true;
  }

  @Override
  public boolean offerLast(T t) {
    addLast(t);
    return true;
  }

  @Override
  public T removeFirst() {
    T value = this.elements.remove(headIndex);
    this.tailIndex--;
    return value;
  }

  @Override
  public T removeLast() {
    T value = this.elements.remove(tailIndex - 1);
    this.tailIndex--;
    return value;
  }

  @Override
  public T pollFirst() {
    if (this.elements.isEmpty()) {
      return null;
    }
    return this.removeFirst();
  }

  @Override
  public T pollLast() {
    if (this.elements.isEmpty()) {
      return null;
    }
    return this.removeLast();
  }

  @Override
  public T getFirst() {
    return this.elements.get(headIndex);
  }

  @Override
  public T getLast() {
    return this.elements.get(tailIndex - 1);
  }

  @Override
  public T peekFirst() {
    if (this.elements.isEmpty()) {
      return null;
    }
    return this.getFirst();
  }

  @Override
  public T peekLast() {
    if (this.elements.isEmpty()) {
      return null;
    }
    return this.getLast();
  }

  @Override
  public boolean removeFirstOccurrence(Object o) {
    for (int i = 0; i < elements.size(); i++) {
      if (elements.get(i).equals(o)) {
        this.elements.remove(i);
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean removeLastOccurrence(Object o) {
    for (int i = elements.size() - 1; i >= 0; i--) {
      if (elements.get(i).equals(o)) {
        this.elements.remove(i);
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean add(T t) {
    return false;
  }

  @Override
  public boolean offer(T t) {
    return false;
  }

  @Override
  public T remove() {
    return null;
  }

  @Override
  public T poll() {
    return null;
  }

  @Override
  public T element() {
    return null;
  }

  @Override
  public T peek() {
    return null;
  }

  @Override
  public void push(T t) {
    this.elements.add(tailIndex, t);
    tailIndex++;
  }

  @Override
  public T pop() {
    T value = this.elements.remove(headIndex);
    tailIndex--;
    return value;
  }

  @Override
  public boolean remove(Object o) {
    if (this.elements.remove(o)) {
      tailIndex--;
      return true;
    }
    return false;
  }

  @Override
  public boolean contains(Object o) {
    return this.elements.contains(o);
  }

  @Override
  public int size() {
    return this.elements.size();
  }

  @Override
  public Iterator<T> iterator() {
    return this.elements.iterator();
  }

  @Override
  public Iterator<T> descendingIterator() {
    return this.elements.iterator();
  }

  @Override
  public boolean isEmpty() {
    return this.elements.isEmpty();
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public <T1> T1[] toArray(T1[] a) {
    return null;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean addAll(Collection<? extends T> c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return false;
  }

  @Override
  public void clear() {
    this.elements.clear();
  }
}
