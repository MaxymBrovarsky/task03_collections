package com.brom.deque;

import java.util.Deque;
import java.util.LinkedList;

public class Main {
  private static int[] testData = {3,4,5,6,-14,-5,-6,7,9,0};
  public static void main(String[] args) {
    MyDeque<Integer> myDeque = new MyDeque<>();
    Deque<Integer> deque = new LinkedList<>();
    initDeque(myDeque);
    initDeque(deque);
    printDeque(myDeque);
    printDeque(deque);
    myDeque.pollLast();
    deque.pollLast();
    printDeque(myDeque);
    printDeque(deque);
    myDeque.pollFirst();
    deque.pollFirst();
    printDeque(myDeque);
    printDeque(deque);
    System.out.println("---------------------");
    System.out.println(myDeque.getFirst());
    System.out.println(deque.getFirst());
    System.out.println(myDeque.getLast());
    System.out.println(deque.getLast());
  }
  private static void initDeque(Deque<Integer> deque) {
    for (int i = 0; i < testData.length / 2; i++) {
      deque.addFirst(testData[i]);
    }
    for (int i = testData.length / 2; i < testData.length; i++) {
      deque.addLast(testData[i]);
    }
  }
  private static void printDeque(Deque deque) {
    System.out.println("--------" + deque.getClass().getName() + "---------------");
    deque.forEach(e -> {
      System.out.println(e);
    });
  }
}
